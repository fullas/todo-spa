import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { ToDo } from './../../todo.model';
import { TodoService } from './../../services/todo.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit, OnDestroy {
  @Input() todo: ToDo;

  isEmpty = true;
  todoView = 'all';

  set todos(value: ToDo[]) {
    this._todos = value;
    this.isEmpty = this.todos.length == 0;
  }
  get todos(): ToDo[] {
    return this._todos;
  }
  private _todos: ToDo[] = [];

  updateViewSub: Subscription;
  toViewSub: Subscription;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.updateViewSub = this.todoService.updateView.subscribe(
      (data) => {
        this.todos = data;
      }
    );
    this.toViewSub = this.todoService.toView.subscribe(
      (data) => {
        this.todoView = data;
      }
    );
  }

  ngOnDestroy() {
    this.updateViewSub.unsubscribe();
    this.toViewSub.unsubscribe();
  }

  onCheck(todo: ToDo) {
    this.todoService.update(todo.id);
    if (this.todos.length < 1) {
      this.todoView = 'all';
      this.todoService.toView.next(this.todoView);
    }
    this.todoService.showTodos(this.todoView);
  }

  onDelete(todo: ToDo) {
    this.todoService.delete(todo.id);
    if (!this.todos.length) {
      this.todoView = 'all';
      this.todoService.toView.next(this.todoView);
    }
    this.todoService.showTodos(this.todoView);
  }

}
