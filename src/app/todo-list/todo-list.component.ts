import { Component, OnInit, OnDestroy } from '@angular/core';

import { ToDo } from './../todo.model';
import { TodoService } from './../services/todo.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {
  isEmpty = true;

  set todos(value: ToDo[]) {
    this._todos = value;
    this.isEmpty = this.todos.length == 0;
  }
  get todos(): ToDo[] {
    return this._todos;
  }
  private _todos: ToDo[] = [];

  updateViewSub: Subscription;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todos = this.todoService.getArray();
    this.updateViewSub = this.todoService.updateView.subscribe(
      (data) => {
        this.todos = data;
      }
    );
  }

  ngOnDestroy() {
    this.updateViewSub.unsubscribe();
  }
}
