export class ToDo {
  public id: number;
  public name: string;
  public date: Date;
  public checked: boolean;

  constructor(name: string, date: Date) {
    this.name = name;
    this.date = date;
    this.checked = false;

    this.id = Date.now();
  }
}
