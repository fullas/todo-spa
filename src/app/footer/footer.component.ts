import { Component, OnInit, OnDestroy } from '@angular/core';

import { ToDo } from './../todo.model';
import { TodoService } from './../services/todo.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {
  todos: ToDo[];
  isOpen = false;
  todoView = 'all';
  info : number[];

  liveCountSub: Subscription;
  toViewSub: Subscription;

  constructor(private todoService: TodoService) {
    $(document).ready(function() {
      $('#menu-icon').click(function(){
        $('ul').toggleClass('opened');
      });

      $('.footer-button').click(function() {
        $('ul').removeClass('opened');
      });

      $('#cancelBtn').click(function(){
        $('ul').removeClass('opened');
      });
    });
  }

  ngOnInit() {
    this.info = this.todoService.getInfo();
    this.liveCountSub = this.todoService.liveCount.subscribe(
      (data) => {
        this.info = data;
      }
    );
    this.toViewSub = this.todoService.toView.subscribe(
      (string) => {
        this.todoView = string;
      }
    );
  }

  ngOnDestroy() {
    this.liveCountSub.unsubscribe();
    this.toViewSub.unsubscribe();
  }

  onRemoveAll() {
    this.todoView = 'all';
    this.todoService.deleteAll();
    this.todos = this.todoService.getArray();
    this.isOpen = false;
    this.todoService.updateView.next(this.todos);
  }

  toggleMenu() {
    this.isOpen = !this.isOpen;
  }

  onListAll() {
    this.todoView = 'all';
    this.isOpen = false;
    this.todos = this.todoService.getAll();
    this.todoService.updateView.next(this.todos);
  }
  onListCompleted() {
    this.todoView = 'completed';
    this.isOpen = false;
    this.todos = this.todoService.getCompleted();
    this.todoService.updateView.next(this.todos);
  }
  onListRemaining() {
    this.todoView = 'remaining';
    this.isOpen = false;
    this.todos = this.todoService.getRemaining();
    this.todoService.updateView.next(this.todos);
  }

}
