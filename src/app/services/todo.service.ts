import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { BaseService } from './base.service';
import { ToDo } from '../todo.model';

@Injectable()
export class TodoService {
  updateView = new Subject<ToDo[]>();
  toView = new Subject<string>();
  liveCount = new Subject<number[]>();

  todoView = 'all';

  constructor(private baseService: BaseService) { }

  private getObj(): ToDo {
    return this.baseService.get('todos');
  }

  getArray(): ToDo[] {
    const items = this.getObj();
    let array = Object.keys(items).map(id => items[id]);
    return array || [];
  }

  getInfo(): number[] {
    const info: number[] = [];
    const all = this.getArray();
    const complete = all.filter(todo => todo.checked).length;
    const remained = all.length - complete;
    info.push(all.length, complete, remained);
    this.liveCount.next(info);
    return info;
  }

  add(todo: ToDo) {
    const todos = this.getObj();
    todos[todo.id] = todo;

    this.baseService.set('todos', todos);
    this.getArray();
    this.showTodos(this.todoView);
  }

  update(id: number) {
    const todos = this.getObj();
    todos[id].checked = !todos[id].checked;

    this.baseService.set('todos', todos);
    this.getArray();

    this.showTodos(this.todoView);
  }

  delete(id: number) {
    const todos = this.getObj();

    delete todos[id];

    this.baseService.set('todos', todos);
    this.getArray();
    this.showTodos(this.todoView);
  }

  deleteAll() {
    this.todoView = 'all';
    this.baseService.set('todos', {});
    this.getArray();
    this.showTodos(this.todoView);
  }

  getAll(): ToDo[] {
    this.todoView = 'all';
    const all = this.getArray();
    this.toView.next(this.todoView);
    return all;
  }

  getCompleted(): ToDo[] {
    this.todoView = 'completed';
    const completed = this.getArray().filter(todo => todo.checked);
    this.toView.next(this.todoView);
    return completed;
  }

  getRemaining(): ToDo[] {
    this.todoView = 'remaining';
    const remaining = this.getArray().filter(todo => !todo.checked);
    this.toView.next(this.todoView);
    return remaining;
  }

  showTodos(todoView) {
    switch (todoView) {
      case 'all': {
        const todos = this.getArray();
        this.updateView.next(todos);
        this.getInfo();
        break;
      }
      case 'completed': {
        const completed = this.getCompleted();
        if (completed.length === 0) {
          this.todoView = 'all';
          this.showTodos(this.todoView);
        }
        this.updateView.next(completed);
        this.getInfo();
        break;
      }
      case 'remaining': {
        const remaining = this.getRemaining();
        if (remaining.length === 0) {
          this.todoView = 'all';
          this.showTodos(this.todoView);
        }
        this.updateView.next(remaining);
        this.getInfo();
        break;
      }
      default: {
        const todos = this.getArray();
        this.updateView.next(todos);
        this.getInfo();
        break;
      }
    }
  }
}
