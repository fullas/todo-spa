import { Injectable } from '@angular/core';

@Injectable()
export class BaseService {

  private key = 'myTodos';

  get(key: string) {
    const data = JSON.parse(localStorage.getItem(this.key)) || {};

    return data[key] || {};
  }

  set(key: string, value: any) {
    const data = JSON.parse(localStorage.getItem(this.key)) || {};

    data[key] = value;

    localStorage.setItem(this.key, JSON.stringify(data));
  }
}
