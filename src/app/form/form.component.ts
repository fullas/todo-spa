import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ToDo } from './../todo.model';
import { TodoService } from './../services/todo.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild('f') myForm: NgForm;
  @ViewChild('input') input: ElementRef;

  todo: ToDo;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
  }

  onSubmit() {
    const newDate = new Date();
    const name = this.myForm.value.toDo;
    if (this.myForm.valid) {
      this.todo = new ToDo(name, newDate);
      this.todoService.add(this.todo);
      this.input.nativeElement.blur();
      this.myForm.resetForm();
    } else {
      this.input.nativeElement.blur();
      this.myForm.resetForm();
    }
    this.todoService.getInfo();
  }

}
