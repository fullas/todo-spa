import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  date: Date;

  constructor() { }

  ngOnInit() {
    this.date = new Date;
    setInterval(() => {
    this.date =  new Date();
  }, 1000);
  }

}
